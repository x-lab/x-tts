use cpp::cpp;
// use qmetaobject::prelude::*;

cpp! {{
#include <xTTS.hpp>
}}

pub fn init() {

    cpp!(unsafe [] {
        x_tts_initialise();
    });
}
