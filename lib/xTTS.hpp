#pragma once

#include "qtexttospeech.h"

#include <QtCore>
#include <QtQml>

class xSpeechEngine : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList languages READ languages NOTIFY languagesChanged)
    Q_PROPERTY(QStringList    voices READ    voices NOTIFY    voicesChanged)

public:
     xSpeechEngine(QObject *parent = nullptr);
    ~xSpeechEngine(void);

public:
    QStringList languages(void)
    {
        return l_languages;
    }
    QStringList voices(void)
    {
        return l_voices;
    }

signals:
    void languagesChanged(void);
    void    voicesChanged(void);
    void status(const QString& message);

public slots:
    void speak(const QString&);
    void stop(void);

    void pause(void)
    {
        m_speech->pause();
    }

    void resume(void)
    {
        m_speech->resume();
    }

    void setRate(double);
    void setPitch(double);
    void setVolume(int);

    void stateChanged(QTextToSpeech::State state);
    void engineSelected(const QString&);
    void languageSelected(const QString&);
    void voiceSelected(const QString&);

    void localeChanged(const QLocale &locale);

private:
    QTextToSpeech *m_speech;
    QVector<QVoice> m_voices;

    QStringList l_languages;
    QStringList l_voices;

    QVariantMap v_languages;
};

void x_tts_initialise(void);
