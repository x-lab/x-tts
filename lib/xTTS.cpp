#include "xTTS.hpp"

#include <QLoggingCategory>

#if defined(Q_OS_MAC)
Q_IMPORT_PLUGIN(QTextToSpeechPluginOsx);
#endif

xSpeechEngine::xSpeechEngine(QObject *parent) : QObject(parent), m_speech(0)
{
    QLoggingCategory::setFilterRules(QStringLiteral("qt.speech.tts=true \n qt.speech.tts.*=true"));

#if defined(Q_OS_MAC)
    engineSelected("osx");
#endif

    connect(m_speech, &QTextToSpeech::stateChanged, this, &xSpeechEngine::stateChanged);
}

xSpeechEngine::~xSpeechEngine(void)
{
    delete m_speech;
}

void xSpeechEngine::speak(const QString& text)
{
    m_speech->say(text);
}

void xSpeechEngine::stop(void)
{
    m_speech->stop();
}

void xSpeechEngine::setRate(double rate)
{
    // qDebug() << Q_FUNC_INFO << rate;

    m_speech->setRate(rate / 10.0);
}

void xSpeechEngine::setPitch(double pitch)
{
    // qDebug() << Q_FUNC_INFO << pitch;

    m_speech->setPitch(pitch / 10.0);
}

void xSpeechEngine::setVolume(int volume)
{
    // qDebug() << Q_FUNC_INFO << volume;

    m_speech->setVolume(volume);
}

void xSpeechEngine::stateChanged(QTextToSpeech::State state)
{
    if (state == QTextToSpeech::Speaking)
        emit status("Speech started...");
    else if (state == QTextToSpeech::Ready)
        emit status("Speech stopped...");
    else if (state == QTextToSpeech::Paused)
        emit status("Speech paused...");
    else
        emit status("Speech error!");
}

void xSpeechEngine::engineSelected(const QString& engineName)
{
    delete m_speech;

    if (engineName == "default")
        m_speech = new QTextToSpeech(this);
    else
        m_speech = new QTextToSpeech(engineName, QVariantMap(), this);

    l_languages.clear();
    v_languages.clear();

    QVector<QLocale> locales = m_speech->availableLocales();

    QLocale current = m_speech->locale();
    foreach (const QLocale &locale, locales) {

        QString name(QString("%1 (%2)")
                     .arg(QLocale::languageToString(locale.language()))
                     .arg(QLocale::countryToString(locale.country())));
        QVariant localeVariant(locale);
        l_languages << name;
        v_languages[name] = localeVariant;
        if (locale.name() == current.name())
            current = locale;
    }

    emit languagesChanged();

    // setRate(ui.volume->value());
    // setPitch(ui.pitch->value());

    m_speech->setVolume(100);

    // connect(ui.volume, &QSlider::valueChanged, m_speech, &QTextToSpeech::setVolume);

    localeChanged(current);
}

void xSpeechEngine::languageSelected(const QString& language)
{
    QLocale locale = v_languages[language].toLocale();

    m_speech->setLocale(locale);

localeChanged(locale);
}

void xSpeechEngine::voiceSelected(const QString& voice)
{
    m_speech->setVoice(m_voices[l_voices.indexOf(voice)]);
}

void xSpeechEngine::localeChanged(const QLocale &locale)
{
    QVariant localeVariant(locale);

    l_voices.clear();

    m_voices = m_speech->availableVoices();
    QVoice currentVoice = m_speech->voice();
    foreach (const QVoice &voice, m_voices) {
        l_voices << QString("%1 - %2 - %3").arg(voice.name())
                          .arg(QVoice::genderName(voice.gender()))
                          .arg(QVoice::ageName(voice.age()));
    }

    emit voicesChanged();
}

void x_tts_initialise(void)
{
    qmlRegisterType<xSpeechEngine>("xQuick.TTS", 1, 0, "TTSEngine");
}
